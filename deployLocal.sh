#!/bin/bash
#This script will help a deployment of a docker image on an MBB bigmem machine

if [ $# -lt 2 ]
then
  echo usage : $0 dataDir resultsDir '[dockerHub|local]'
  exit
fi

#essayer une plage de ports entre 8787 et 8800
#APP_PORT=$2 
APP_PORT=8787
while [[ $(ss -tulw | grep $APP_PORT) != "" && $APP_PORT < 8800 ]]
do
  APP_PORT=$(( $APP_PORT + 1))
done

if [[ $(ss -tulw | grep $APP_PORT) != "" ]]
then
  echo "No tcp port available !!"
  exit -1
fi

# Docker volumes
# MBB Workflows reads data from /Data and write results to /Results

if [ $SUDO_USER ]; then realUSER=$SUDO_USER; else realUSER=`whoami`; fi

Data=$1
Results=$2

mkdir -p $Data
mkdir -p $Results

DOCK_VOL+=" --mount type=bind,src=$Data,dst=/Data"
DOCK_VOL+=" --mount type=bind,src=$Results,dst=/Results"

if [ $# -lt 3 ]
then
    APP_IMG="mbbteam/radseqref:latest"  
else 
    IMG_SRC=$3
    case $IMG_SRC in
        dockerHub )
            APP_IMG="mbbteam/radseqref:latest"  ;;
        local)
            docker build . -t radseqref:latest 
            APP_IMG="radseqref:latest" ;;
        mbb)    
            #APP_IMG="X.X.X.X:5000/radseqref:latest" ;;
    esac
fi

CONTAINER_ID=$( docker run --rm -d -p $APP_PORT:3838 $DOCK_VOL $APP_IMG )
if [ $CONTAINER_ID ]
then
    echo " "
    echo You have to put your Data on  : $1
    echo " "
    echo Results will be written to    : $2
    echo " "
    #hostname -I | grep -E -o "162.38.181.[0-9]{1,3}" | awk -v port=$APP_PORT '{print "You can access the workflow interface at :  http://"$1":"port}'
    echo  You can access the workflow interface at :  http://127.0.0.1:${APP_PORT}

    echo " "
    echo To start a Bash session inside the container : docker exec -it $CONTAINER_ID /bin/bash
else
    echo Failed to run the docker container  !!
fi
