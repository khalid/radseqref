#!/bin/bash
#This script will help to run a workflow in a docker image. 

if [ $# -lt 4 ]
then
  echo usage : $0 dataDir resultsDir configFile nbCores '[dockerHub|local]'
  exit
fi

# Docker volumes
# MBB Workflows reads data from /Data and write results to /Results

Data=$1
Results=$2

if [ ! -d "$Data" ]; then
  echo "can't find $Data directory !"
  exit;
fi

mkdir -p $Results

DOCK_VOL+=" --mount type=bind,src=$Data,dst=/Data"
DOCK_VOL+=" --mount type=bind,src=$Results,dst=/Results"

# config file must be in /Data or /Results !
config=$3

cores=$4

if [ $# -lt 5 ]
then
    APP_IMG="mbbteam/radseqref:latest"  
else 
    IMG_SRC=$5
    case $IMG_SRC in
        dockerHub )
            APP_IMG="mbbteam/radseqref:latest"  ;;
        local)
            docker build . -t radseqref:latest 
            APP_IMG="radseqref:latest" ;;
        mbb)    
            #APP_IMG="X.X.X.X:5000/radseqref:latest" ;;
    esac
fi

docker run --rm $DOCK_VOL --cidfile="CID.txt"  $APP_IMG snakemake -s /workflow/Snakefile all --configfile $config --cores $cores

CONTAINER_ID=$(cat CID.txt)
if [ $CONTAINER_ID ]
then
    echo " "
    echo Results were written to    : $2
    echo " "
else
    echo Failed to run the docker container  !!
fi
